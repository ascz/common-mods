class CfgWeapons
{
    class Pistol_Base_F;
    class RH_Pistol_Base_F: Pistol_Base_F
 	{
        dfyre_soundeffect = "DFyre_A_Core_Effects_DF_WP_fnp";
 	};
    class RH_deagle: RH_Pistol_Base_F
    {
        jsrs_soundeffect = "JSRS2_Distance_Effects_rook40";
    };
    class RH_mateba: Pistol_Base_F
    {
        jsrs_soundeffect = "JSRS2_Distance_Effects_rook40";
    };
    class RH_mp412: Pistol_Base_F
    {
        jsrs_soundeffect = "JSRS2_Distance_Effects_rook40";
    };
    class RH_python: Pistol_Base_F
    {
        jsrs_soundeffect = "JSRS2_Distance_Effects_rook40";
    };
	class RH_ttracker: Pistol_Base_F
	{
        jsrs_soundeffect = "JSRS2_Distance_Effects_rook40";
    };
    class RH_kimber: RH_Pistol_Base_F
    {
        jsrs_soundeffect = "JSRS2_Distance_Effects_acp";
    };
    class RH_m1911: RH_Pistol_Base_F
    {
        jsrs_soundeffect = "JSRS2_Distance_Effects_1911";
    };
    class RH_m9: RH_Pistol_Base_F
    {
        jsrs_soundeffect = "JSRS2_Distance_Effects_M9";
    };
    class RH_g18: RH_Pistol_Base_F
    {
        jsrs_soundeffect = "JSRS2_Distance_Effects_Glock";
    };
    class RH_g17: RH_Pistol_Base_F
    {
        jsrs_soundeffect = "JSRS2_Distance_Effects_Glock";
    };
    class RH_g19: RH_Pistol_Base_F
    {
        jsrs_soundeffect = "JSRS2_Distance_Effects_Glock";
    };
   	class RH_vz61: RH_Pistol_Base_F
    {
        dfyre_soundeffect = "DFyre_A_Core_Effects_DF_WP_SMG";
    };
   	class RH_tec9: RH_Pistol_Base_F
    {
        dfyre_soundeffect = "DFyre_A_Core_Effects_DF_WP_SMG";
    };
   	class RH_muzi: RH_Pistol_Base_F
    {
        dfyre_soundeffect = "DFyre_A_Core_Effects_DF_WP_SMG";
    };
};