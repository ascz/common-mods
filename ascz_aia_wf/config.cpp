class CfgPatches
{
	class ascz_aia_wf
	{
	    units[] = {};
        worlds[] = {};
        weapons[] = {};
		requiredVersion = 1.0;
		requiredAddons[] = {"AiA_StandaloneTerrainPack_Core_Faction"};
		author[] = {"EvroMalarkey"};
		version = "1.0";
	};
};

class CfgVehicles {
	class NonStrategic;
    class Base_WarfareBBarracks;
    class USMC_WarfareBBarracks: Base_WarfareBBarracks
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class RU_WarfareBBarracks: Base_WarfareBBarracks
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class CDF_WarfareBBarracks: Base_WarfareBBarracks
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class Ins_WarfareBBarracks: Base_WarfareBBarracks
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class Gue_WarfareBBarracks: Base_WarfareBBarracks
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class Base_WarfareBLightFactory;
    class USMC_WarfareBLightFactory: Base_WarfareBLightFactory
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class RU_WarfareBLightFactory: Base_WarfareBLightFactory
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class CDF_WarfareBLightFactory: Base_WarfareBLightFactory
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class Ins_WarfareBLightFactory: Base_WarfareBLightFactory
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class Gue_WarfareBLightFactory: Base_WarfareBLightFactory
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class Base_WarfareBHeavyFactory;
    class USMC_WarfareBHeavyFactory: Base_WarfareBHeavyFactory
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class RU_WarfareBHeavyFactory: Base_WarfareBHeavyFactory
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class CDF_WarfareBHeavyFactory: Base_WarfareBHeavyFactory
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class Ins_WarfareBHeavyFactory: Base_WarfareBHeavyFactory
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class Gue_WarfareBHeavyFactory: Base_WarfareBHeavyFactory
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class Base_WarfareBAircraftFactory;
    class USMC_WarfareBAircraftFactory: Base_WarfareBAircraftFactory
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class RU_WarfareBAircraftFactory: Base_WarfareBAircraftFactory
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class CDF_WarfareBAircraftFactory: Base_WarfareBAircraftFactory
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class WarfareBAircraftFactory_CDF: Base_WarfareBAircraftFactory
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class INS_WarfareBAircraftFactory: Base_WarfareBAircraftFactory
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class WarfareBAircraftFactory_Ins: Base_WarfareBAircraftFactory
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class GUE_WarfareBAircraftFactory: Base_WarfareBAircraftFactory
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class WarfareBAircraftFactory_Gue: Base_WarfareBAircraftFactory
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class BASE_WarfareBFieldhHospital;
    class USMC_WarfareBFieldhHospital: BASE_WarfareBFieldhHospital
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class RU_WarfareBFieldhHospital: BASE_WarfareBFieldhHospital
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class CDF_WarfareBFieldhHospital: BASE_WarfareBFieldhHospital
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class INS_WarfareBFieldhHospital: BASE_WarfareBFieldhHospital
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class GUE_WarfareBFieldhHospital: BASE_WarfareBFieldhHospital
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class BASE_WarfareBAntiAirRadar;
    class USMC_WarfareBAntiAirRadar: BASE_WarfareBAntiAirRadar
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class RU_WarfareBAntiAirRadar: BASE_WarfareBAntiAirRadar
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class CDF_WarfareBAntiAirRadar: BASE_WarfareBAntiAirRadar
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class INS_WarfareBAntiAirRadar: BASE_WarfareBAntiAirRadar
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class GUE_WarfareBAntiAirRadar: BASE_WarfareBAntiAirRadar
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class BASE_WarfareBArtilleryRadar;
    class USMC_WarfareBArtilleryRadar: BASE_WarfareBArtilleryRadar
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class RU_WarfareBArtilleryRadar: BASE_WarfareBArtilleryRadar
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class CDF_WarfareBArtilleryRadar: BASE_WarfareBArtilleryRadar
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class Ins_WarfareBArtilleryRadar: BASE_WarfareBArtilleryRadar
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class Gue_WarfareBArtilleryRadar: BASE_WarfareBArtilleryRadar
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class Base_WarfareBUAVterminal;
    class USMC_WarfareBUAVterminal: Base_WarfareBUAVterminal
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class RU_WarfareBUAVterminal: Base_WarfareBUAVterminal
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class CDF_WarfareBUAVterminal: Base_WarfareBUAVterminal
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class INS_WarfareBUAVterminal: Base_WarfareBUAVterminal
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class GUE_WarfareBUAVterminal: Base_WarfareBUAVterminal
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class Base_WarfareBVehicleServicePoint;
    class USMC_WarfareBVehicleServicePoint: Base_WarfareBVehicleServicePoint
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class RU_WarfareBVehicleServicePoint: Base_WarfareBVehicleServicePoint
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class CDF_WarfareBVehicleServicePoint: Base_WarfareBVehicleServicePoint
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class INS_WarfareBVehicleServicePoint: Base_WarfareBVehicleServicePoint
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class GUE_WarfareBVehicleServicePoint: Base_WarfareBVehicleServicePoint
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class Warfare_HQ_base_unfolded;
    class BRDM2_HQ_Gue_unfolded: Warfare_HQ_base_unfolded
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class BTR90_HQ_unfolded: Warfare_HQ_base_unfolded
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class LAV25_HQ_unfolded: Warfare_HQ_base_unfolded
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class BMP2_HQ_INS_unfolded: Warfare_HQ_base_unfolded
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class BMP2_HQ_CDF_unfolded: Warfare_HQ_base_unfolded
    {
        delete faction;
        delete side;
        scope = 2;
        scopeCurator = 2;
    };
    class WarfareBunkerSign: NonStrategic
    {
        scope = 2;
        scopeCurator = 2;
    };

    class US_WarfareBBarracks_Base_EP1: Base_WarfareBBarracks
    {
        scopeCurator = 2;
    };
    class TK_WarfareBBarracks_Base_EP1: Base_WarfareBBarracks
    {
        scopeCurator = 2;
    };
    class TK_GUE_WarfareBBarracks_Base_EP1: Base_WarfareBBarracks
    {
        scopeCurator = 2;
    };
    class Base_WarfareBContructionSite;
    class US_WarfareBContructionSite_Base_EP1: Base_WarfareBContructionSite
    {
        scopeCurator = 2;
    };
    class TK_WarfareBContructionSite_Base_EP1: Base_WarfareBContructionSite
    {
        scopeCurator = 2;
    };
    class TK_GUE_WarfareBContructionSite_Base_EP1: Base_WarfareBContructionSite
    {
        scopeCurator = 2;
    };
    class US_WarfareBLightFactory_base_EP1: Base_WarfareBLightFactory
    {
        scopeCurator = 2;
    };
    class TK_WarfareBLightFactory_base_EP1: Base_WarfareBLightFactory
    {
        scopeCurator = 2;
    };
    class TK_GUE_WarfareBLightFactory_base_EP1: Base_WarfareBLightFactory
    {
        scopeCurator = 2;
    };
    class US_WarfareBHeavyFactory_Base_EP1: Base_WarfareBHeavyFactory
    {
        scopeCurator = 2;
    };
    class TK_WarfareBHeavyFactory_Base_EP1: Base_WarfareBHeavyFactory
    {
        scopeCurator = 2;
    };
    class TK_GUE_WarfareBHeavyFactory_Base_EP1: Base_WarfareBHeavyFactory
    {
        scopeCurator = 2;
    };
    class US_WarfareBAircraftFactory_Base_EP1: Base_WarfareBAircraftFactory
    {
        scopeCurator = 2;
    };
    class TK_WarfareBAircraftFactory_Base_EP1: Base_WarfareBAircraftFactory
    {
        scopeCurator = 2;
    };
    class TK_GUE_WarfareBAircraftFactory_Base_EP1: Base_WarfareBAircraftFactory
    {
        scopeCurator = 2;
    };
    class US_WarfareBFieldhHospital_Base_EP1: BASE_WarfareBFieldhHospital
    {
        scopeCurator = 2;
    };
    class TK_WarfareBFieldhHospital_Base_EP1: BASE_WarfareBFieldhHospital
    {
        scopeCurator = 2;
    };
    class TK_GUE_WarfareBFieldhHospital_Base_EP1: BASE_WarfareBFieldhHospital
    {
        scopeCurator = 2;
    };
    class US_WarfareBAntiAirRadar_Base_EP1: BASE_WarfareBAntiAirRadar
    {
        scopeCurator = 2;
    };
    class TK_WarfareBAntiAirRadar_Base_EP1: BASE_WarfareBAntiAirRadar
    {
        scopeCurator = 2;
    };
    class TK_GUE_WarfareBAntiAirRadar_Base_EP1: BASE_WarfareBAntiAirRadar
    {
        scopeCurator = 2;
    };
    class US_WarfareBArtilleryRadar_Base_EP1: BASE_WarfareBArtilleryRadar
    {
        scopeCurator = 2;
    };
    class TK_WarfareBArtilleryRadar_Base_EP1: BASE_WarfareBArtilleryRadar
    {
        scopeCurator = 2;
    };
    class TK_GUE_WarfareBArtilleryRadar_Base_EP1: BASE_WarfareBArtilleryRadar
    {
        scopeCurator = 2;
    };
    class US_WarfareBUAVterminal_Base_EP1: Base_WarfareBUAVterminal
    {
        scopeCurator = 2;
    };
    class TK_WarfareBUAVterminal_Base_EP1: Base_WarfareBUAVterminal
    {
        scopeCurator = 2;
    };
    class TK_GUE_WarfareBUAVterminal_Base_EP1: Base_WarfareBUAVterminal
    {
        scopeCurator = 2;
    };
    class US_WarfareBVehicleServicePoint_Base_EP1: Base_WarfareBVehicleServicePoint
    {
        scopeCurator = 2;
    };
    class TK_WarfareBVehicleServicePoint_Base_EP1: Base_WarfareBVehicleServicePoint
    {
        scopeCurator = 2;
    };
    class TK_GUE_WarfareBVehicleServicePoint_Base_EP1: Base_WarfareBVehicleServicePoint
    {
        scopeCurator = 2;
    };
    class BRDM2_HQ_TK_GUE_unfolded_Base_EP1: Warfare_HQ_base_unfolded
    {
        scopeCurator = 2;
    };
    class M1130_HQ_unfolded_Base_EP1: Warfare_HQ_base_unfolded
    {
        scopeCurator = 2;
    };
    class BMP2_HQ_TK_unfolded_Base_EP1: Warfare_HQ_base_unfolded
    {
        scopeCurator = 2;
    };
};