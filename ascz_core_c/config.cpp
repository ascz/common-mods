class CfgPatches
{
	class ascz_core_c
	{
		weapons[] = {};
		units[] = {"ascz_arsenal_box"};
		requiredVersion = 1.0;
		requiredAddons[] = {"A3_Structures_F_EPA_Mil_Scrapyard"};
		version = "1.0";
		author = "EvroMalarkey";
	};
};


class CfgVehicles {

	class Scrapyard_base_F;
    class Land_PaperBox_open_full_F: Scrapyard_base_F
    {
        maximumLoad = 2000;
        transportMaxWeapons = 12;
        transportMaxMagazines = 64;
        transportMaxBackpacks = 12;
		supplyRadius = 1.4;
        class TransportMagazines {};
        class TransportWeapons {};
        class TransportItems {};
    };

    class ascz_arsenal_box: Land_PaperBox_open_full_F
    {
            displayName = "[ASCZ] Arsenal";
            vehicleClass = "Ammo";
            class eventHandlers
            {
                init = "_this select 0 addAction [""<img image='\A3\Ui_f\data\Logos\arsenal_1024_ca.paa' size='1.8' shadow=0.1 />"", { [""Open"", true] call BIS_fnc_arsenal; }, """", 6];";
            };
    };
};
