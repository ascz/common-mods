class CfgPatches
{
	class ASCZ_Heads
	{
        author = "Taurus";
        hideName=0;
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Characters_F","A3_Characters_F_beta","A3_Characters_F_epa","A3_Characters_F_epb","A3_Characters_F_epc","A3_Characters_F_exp"};
	};
};
class CfgFaces
{
	class Default
	{
		class Custom;
	};
	class Man_A3: Default
	{
		class Default;
		class WhiteHead_01;
		class AfricanHead_01;
		class AsianHead_A3_01;
		class GreekHead_A3_01;
		class TanoanHead_A3_01;
		class Custom
		{
			material = "ASCZ_Heads\data\Custom.rvmat";
		};
		class Miller: Default
		{
			displayname = "Miller";
			head = "Miller";
			disabled = 0;
			author = "$STR_A3_Bohemia_Interactive";
		};
		class TanoanBossHead: TanoanHead_A3_01
		{
			author = "$STR_A3_Bohemia_Interactive";
			displayName = "Maru";
			disabled = 0;
			DLC = "Expansion";
		};
		class asczHead_muller_A3: Miller
		{
			disabled = 0;
			displayname = "Müller";
			head = "NATOHead_A3";
			author = "$STR_A3_Bohemia_Interactive";
		};
		class Kerry: Default
		{
			displayname = "Kerry";
			head = "KerryHead_A3";
			disabled = 0;
			author = "$STR_A3_Bohemia_Interactive";
		};
		class kerry_A_F: Kerry
		{
			displayname = "Kerry (Copy)";
			head = "KerryHead_A3";
			disabled = 1;
			author = "$STR_A3_Bohemia_Interactive";
		};
		class kerry_B1_F: Kerry
		{
			displayname = "Kerry (2)";
			author = "$STR_A3_Bohemia_Interactive";
			disabled = 0;
		};
		class kerry_B2_F: Kerry
		{
			displayname = "Kerry (3)";
			author = "$STR_A3_Bohemia_Interactive";
			disabled = 0;
		};
		class Kerry_C_F: Kerry
		{
			displayname = "Kerry (4)";
			author = "$STR_A3_Bohemia_Interactive";
			disabled = 0;
		};
		class IG_Leader: Default
		{
			disabled = 0;
			displayname = "Stavrou";
			head = "IG_Leader";
			author = "$STR_A3_Bohemia_Interactive";
		};
		class O_Colonel: Default
		{
			disabled = 0;
			displayname = "Namdar";
			author = "$STR_A3_Bohemia_Interactive";
			head = "PersianHead_A3";
		};
		class Nikos: GreekHead_A3_01
		{
			displayname = "Panagopoulos";
			head = "Nikos";
			disabled = 0;
			author = "$STR_A3_Bohemia_Interactive";
		};
		class asczHead_troska_A3: WhiteHead_01
		{
			displayname = "Troska";
			texture = "ASCZ_Heads\data\Troska_co.paa";
			head = "NATOHead_A3";
			identityTypes[] = {"Head_Euro","Head_NATO"};
			author = "Taurus";
			material = "ASCZ_Heads\data\Troska.rvmat";
			disabled = 0;
			materialWounded1 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			materialWounded2 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			textureHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
		};
		class asczHead_stick_A3: WhiteHead_01
		{
			displayname = "Stick";
			head = "DefaultHead_A3";
			texture = "ASCZ_Heads\data\Stick_co.paa";
			identityTypes[] = {"Head_Euro","Head_NATO"};
			author = "Taurus";
			materialWounded1 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			materialWounded2 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			material = "ASCZ_Heads\data\Stick.rvmat";
			textureHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
			disabled = 0;
		};
		class asczHead_logan_A3: WhiteHead_01
		{
			displayname = "Logan";
			texture = "ASCZ_Heads\data\Logan_co.paa";
			head = "NATOHead_A3";
			identityTypes[] = {"Head_Euro","Head_NATO"};
			author = "Taurus";
			material = "ASCZ_Heads\data\Logan.rvmat";
			disabled = 0;
			materialWounded1 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			materialWounded2 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			textureHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
		};
		class asczHead_austin_A3: WhiteHead_01
		{
			displayname = "Austin";
			texture = "ASCZ_Heads\data\Austin_co.paa";
			head = "NATOHead_A3";
			identityTypes[] = {"Head_Euro","Head_NATO"};
			author = "Taurus";
			material = "ASCZ_Heads\data\Austin.rvmat";
			disabled = 0;
			materialWounded1 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			materialWounded2 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			textureHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
		};
		class asczHead_obacki_A3: Default
		{
			displayname = "O'Backi";
			texture = "ASCZ_Heads\data\OBacki_co.paa";
			head = "GreekHead_A3";
			identityTypes[] = {"Head_TK"};
			author = "Taurus";
			material = "ASCZ_Heads\data\OBacki.rvmat";
			disabled = 0;
			materialWounded1 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			materialWounded2 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			textureHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
		};
		class asczHead_sokolov_A3: WhiteHead_01
		{
			displayname = "Sokolov";
			head = "DefaultHead_A3";
			material = "ASCZ_Heads\data\Sokolov.rvmat";
			identityTypes[] = {"Head_Euro","Head_NATO"};
			author = "Taurus";
			texture = "ASCZ_Heads\data\Sokolov_co.paa";
			materialWounded1 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			materialWounded2 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			textureHL = "\A3\Characters_F\Heads\Data\hl_white_hairy_2_co.paa";
			materialHL = "\A3\Characters_F_epb\Heads\Data\hl_white_20.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_white_hairy_2_co.paa";
			materialHL2 = "\A3\Characters_F_epb\Heads\Data\hl_white_20.rvmat";
			disabled = 0;
		};
		class asczHead_price_A3: WhiteHead_01
		{
			displayname = "Price";
			texture = "ASCZ_Heads\data\Price_co.paa";
			head = "NATOHead_A3";
			identityTypes[] = {"Head_Euro","Head_NATO"};
			author = "Taurus";
			material = "\A3\Characters_F\Heads\Data\m_greek_03.rvmat";
			disabled = 0;
			materialWounded1 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			materialWounded2 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			textureHL = "\A3\Characters_F\Heads\Data\hl_white_hairy_1_co.paa";
			materialHL = "\A3\Characters_F\Heads\Data\hl_white_old.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_white_hairy_1_co.paa";
			materialHL2 = "\A3\Characters_F\Heads\Data\hl_white_old.rvmat";
		};
		class asczHead_mctavish_A3: WhiteHead_01
		{
			displayname = "MacTavish";
			texture = "ASCZ_Heads\data\MacTavish_co.paa";
			head = "DefaultHead_A3";
			identityTypes[] = {"Head_NATO"};
			author = "Taurus";
			material = "ASCZ_Heads\data\MacTavish.rvmat";
			disabled = 0;
			materialWounded1 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			materialWounded2 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			textureHL = "\A3\Characters_F\Heads\Data\hl_white_bald_co.paa";
			materialHL = "\A3\Characters_F\Heads\Data\hl_white_bald_muscular.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_white_bald_co.paa";
			materialHL2 = "\A3\Characters_F\Heads\Data\hl_white_bald_muscular.rvmat";
		};
		class asczHead_spanel_A3: WhiteHead_01
		{
			displayname = "Spanel";
			texture = "ASCZ_Heads\data\Spanel_co.paa";
			head = "NATOHead_A3";
			identityTypes[] = {"Head_Euro","Head_NATO"};
			author = "Taurus";
			material = "ASCZ_Heads\data\Spanel.rvmat";
			disabled = 0;
			materialWounded1 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			materialWounded2 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			textureHL = "\A3\Characters_F\Heads\Data\hl_white_hairy_2_co.paa";
			materialHL = "\A3\Characters_F_epb\Heads\Data\hl_white_20.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_white_hairy_2_co.paa";
			materialHL2 = "\A3\Characters_F_epb\Heads\Data\hl_white_20.rvmat";
		};
		class asczHead_voodoo_A3: WhiteHead_01
		{
			displayname = "Voodoo";
			head = "KerryHead_A3";
			texture = "ASCZ_Heads\data\Voodoo_co.paa";
			identityTypes[] = {"Head_Euro","Head_NATO"};
			author = "Taurus";
			materialWounded1 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			materialWounded2 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			material = "ASCZ_Heads\data\Voodoo.rvmat";
			textureHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
			disabled = 0;
		};
		class asczHead_beardy_A3: WhiteHead_01
		{
			displayname = "Beardy";
			head = "NATOHead_A3";
			texture = "ASCZ_Heads\data\Beardy_co.paa";
			identityTypes[] = {"Head_Euro","Head_NATO"};
			author = "Taurus";
			material = "ASCZ_Heads\data\Beardy.rvmat";
			materialWounded1 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			materialWounded2 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			textureHL = "\A3\Characters_F\Heads\Data\hl_white_hairy_2_co.paa";
			materialHL = "\A3\Characters_F\Heads\Data\hl_white_old.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_white_hairy_2_co.paa";
			materialHL2 = "\A3\Characters_F\Heads\Data\hl_white_old.rvmat";
			disabled = 0;
		};
		class asczHead_woods_A3: WhiteHead_01
		{
			displayname = "Woods";
			head = "GreekHead_A3";
			texture = "ASCZ_Heads\data\Woods_co.paa";
			identityTypes[] = {"Head_Euro","Head_NATO"};
			author = "Taurus";
			material = "ASCZ_Heads\data\Woods.rvmat";
			materialWounded1 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			materialWounded2 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			textureHL = "\A3\Characters_F\Heads\Data\hl_white_hairy_1_co.paa";
			materialHL = "\A3\Characters_F\Heads\Data\hl_white_old.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_white_hairy_1_co.paa";
			materialHL2 = "\A3\Characters_F\Heads\Data\hl_white_old.rvmat";
			disabled = 0;
		};
		class asczHead_redfield_A3: WhiteHead_01
		{
			displayname = "Redfield";
			head = "NATOHead_A3";
			texture = "ASCZ_Heads\data\Redfield_co.paa";
			identityTypes[] = {"Head_Euro","Head_NATO"};
			author = "Taurus";
			materialWounded1 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			materialWounded2 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			material = "ASCZ_Heads\data\Redfield.rvmat";
			textureHL = "\A3\Characters_F\Heads\Data\hl_white_hairy_1_co.paa";
			materialHL = "\A3\Characters_F_epb\Heads\Data\hl_white_17.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_white_hairy_1_co.paa";
			materialHL2 = "\A3\Characters_F_epb\Heads\Data\hl_white_17.rvmat";
			disabled = 0;
		};
		class asczHead_foley_A3: AfricanHead_01
		{
			displayname = "Foley";
			texture = "ASCZ_Heads\data\Foley_co.paa";
			material = "ASCZ_Heads\data\Foley.rvmat";
			head = "BlackHead_A3";
			identityTypes[] = {"Head_NATO","Head_African","Head_Tanoan"};
			author = "Taurus";
			textureHL = "\A3\Characters_F\Heads\Data\hl_black_bald_co.paa";
			materialHL = "\A3\Characters_F\Heads\Data\hl_black_bald_muscular.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_black_bald_co.paa";
			materialHL2 = "\A3\Characters_F\Heads\Data\hl_black_bald_muscular.rvmat";
			disabled = 0;
		};
		class asczHead_murphy_A3: AfricanHead_01
		{
			displayname = "Murphy";
			texture = "ASCZ_Heads\data\Murphy_co.paa";
			material = "ASCZ_Heads\data\Murphy.rvmat";
			head = "BlackHead_A3";
			identityTypes[] = {"Head_NATO","Head_African","Head_Tanoan"};
			author = "Taurus";
			textureHL = "\A3\Characters_F\Heads\Data\hl_black_bald_co.paa";
			materialHL = "\A3\Characters_F\Heads\Data\hl_black_bald_muscular.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_black_bald_co.paa";
			materialHL2 = "\A3\Characters_F\Heads\Data\hl_black_bald_muscular.rvmat";
			disabled = 0;
		};
		class asczHead_josephe_A3: AfricanHead_01
		{
			displayname = "Josephe";
			texture = "ASCZ_Heads\data\Josephe_co.paa";
			material = "ASCZ_Heads\data\Josephe.rvmat";
			head = "BlackHead_A3";
			identityTypes[] = {"Head_NATO","Head_African","Head_Tanoan"};
			author = "Taurus";
			textureHL = "\A3\Characters_F\Heads\Data\hl_black_bald_co.paa";
			materialHL = "\A3\Characters_F\Heads\Data\hl_black_bald_muscular.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_black_bald_co.paa";
			materialHL2 = "\A3\Characters_F\Heads\Data\hl_black_bald_muscular.rvmat";
			disabled = 0;
		};
		class asczHead_neumann_A3: WhiteHead_01
		{
			displayname = "Neumann";
			texture = "ASCZ_Heads\data\Neumann_co.paa";
			material = "ASCZ_Heads\data\Neumann.rvmat";
			head = "DefaultHead_A3";
			identityTypes[] = {"Head_NATO","Head_Euro"};
			author = "Taurus";
			textureHL = "ASCZ_Heads\data\Neumann_handslegs_co.paa";
			materialHL = "ASCZ_Heads\data\Neumann_handslegs.rvmat";
			textureHL2 = "ASCZ_Heads\data\Neumann_handslegs_co.paa";
			materialHL2 = "ASCZ_Heads\data\Neumann_handslegs.rvmat";
			disabled = 0;
		};
		class asczHead_nevim_A3: WhiteHead_01
		{
			displayname = "Nevim";
			texture = "ASCZ_Heads\data\Nevim_co.paa";
			material = "ASCZ_Heads\data\Nevim.rvmat";
			head = "DefaultHead_A3";
			identityTypes[] = {"Head_NATO","Head_Euro"};
			author = "Taurus";
			materialWounded1 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			materialWounded2 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			textureHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
			disabled = 0;
		};
		class asczHead_novotny_A3: WhiteHead_01
		{
			displayname = "Novotny";
			texture = "ASCZ_Heads\data\Novotny_co.paa";
			material = "ASCZ_Heads\data\Novotny.rvmat";
			head = "DefaultHead_A3";
			identityTypes[] = {"Head_NATO","Head_Euro"};
			author = "Taurus";
			materialWounded1 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			materialWounded2 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			textureHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
			disabled = 0;
		};
		class asczHead_brown_A3: WhiteHead_01
		{
			displayname = "Brown";
			texture = "ASCZ_Heads\data\Brown_co.paa";
			material = "ASCZ_Heads\data\Brown.rvmat";
			head = "DefaultHead_A3";
			identityTypes[] = {"Head_NATO","Head_Euro"};
			author = "Taurus";
			materialWounded1 = "A3\Characters_F\Heads\Data\m_Miller_injury.rvmat";
			materialWounded2 = "A3\Characters_F\Heads\Data\m_Miller_injury.rvmat";
			textureHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
			disabled = 0;
		};
		class asczHead_picleson_A3: WhiteHead_01
		{
			displayname = "Picleson";
			texture = "ASCZ_Heads\data\Picleson_co.paa";
			material = "ASCZ_Heads\data\Picleson.rvmat";
			head = "DefaultHead_A3";
			identityTypes[] = {"Head_NATO","Head_Euro"};
			author = "Taurus";
			materialWounded1 = "A3\Characters_F\Heads\Data\m_Miller_injury.rvmat";
			materialWounded2 = "A3\Characters_F\Heads\Data\m_Miller_injury.rvmat";
			textureHL = "\A3\Characters_F\Heads\Data\hl_white_hairy_nikos_co.paa";
			materialHL = "\A3\Characters_F\Heads\Data\hl_asian_bald_muscular.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_white_hairy_nikos_co.paa";
			materialHL2 = "\A3\Characters_F\Heads\Data\hl_asian_bald_muscular.rvmat";
			disabled = 0;
		};
		class asczHead_vlk_A3: WhiteHead_01
		{
			displayname = "Vlk";
			texture = "ASCZ_Heads\data\Vlk_co.paa";
			material = "ASCZ_Heads\data\Vlk.rvmat";
			head = "GreekHead_A3";
			identityTypes[] = {"Head_NATO","Head_Euro"};
			author = "Taurus";
			materialWounded1 = "A3\Characters_F\Heads\Data\m_Miller_injury.rvmat";
			materialWounded2 = "A3\Characters_F\Heads\Data\m_Miller_injury.rvmat";
			textureHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
			disabled = 0;
		};
		class asczHead_xong_A3: AsianHead_A3_01
		{
			author = "Taurus";
			displayname = "Xong";
			identityTypes[] = {"Head_Asian"};
			head = "AsianHead_A3";
			texture = "ASCZ_Heads\data\Xong_co.paa";
			material = "ASCZ_Heads\data\Xong.rvmat";
			materialWounded1 = "A3\Characters_F\Heads\Data\m_Asian_03_injury.rvmat";
			materialWounded2 = "A3\Characters_F\Heads\Data\m_Asian_03_injury.rvmat";
		};
		class asczHead_olson_A3: WhiteHead_01
		{
			displayname = "Olson";
			texture = "ASCZ_Heads\data\Olson_co.paa";
			material = "ASCZ_Heads\data\Olson.rvmat";
			head = "DefaultHead_A3";
			identityTypes[] = {"Head_NATO","Head_Euro"};
			author = "Taurus";
			materialWounded1 = "A3\Characters_F\Heads\Data\m_Miller_injury.rvmat";
			materialWounded2 = "A3\Characters_F\Heads\Data\m_Miller_injury.rvmat";
			textureHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
			disabled = 0;
		};
		class asczHead_taurus_A3: WhiteHead_01
		{
			displayname = "Taurusin";
			texture = "ASCZ_Heads\data\Taurus_co.paa";
			head = "NATOHead_A3";
			identityTypes[] = {"Head_Euro"};
			author = "Taurus";
			material = "ASCZ_Heads\data\Taurus.rvmat";
			disabled = 0;
			materialWounded1 = "A3\characters_F_EPB\Heads\Data\m_IG_leader_injury.rvmat";
			materialWounded2 = "A3\characters_F_EPB\Heads\Data\m_IG_leader_injury.rvmat";
			textureHL = "ASCZ_Heads\data\Taurus_handslegs_co.paa";
			materialHL = "ASCZ_Heads\data\Taurus_handslegs.rvmat";
			textureHL2 = "ASCZ_Heads\data\Taurus_handslegs_co.paa";
			materialHL2 = "ASCZ_Heads\data\Taurus_handslegs.rvmat";
		};
		class asczHead_snek_A3: WhiteHead_01
		{
			displayname = "Snek";
			texture = "ASCZ_Heads\data\Snek_co.paa";
			head = "GreekHead_A3";
			identityTypes[] = {"Head_Euro"};
			author = "Taurus";
			materialWounded1 = "A3\Characters_F\Heads\Data\m_Miller_injury.rvmat";
			materialWounded2 = "A3\Characters_F\Heads\Data\m_Miller_injury.rvmat";
			textureHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
			material = "ASCZ_Heads\data\Snek.rvmat";
			disabled = 0;
		};
		class asczHead_yompider_A3: Kerry
		{
			displayname = "Yompider";
			texture = "ASCZ_Heads\data\Yompider_co.paa";
			head = "GreekHead_A3";
			identityTypes[] = {""};
			author = "Taurus";
			material = "ASCZ_Heads\data\Yompider.rvmat";
			disabled = 0;
			textureHL = "\A3\Characters_F\Heads\Data\hl_white_hairy_2_co.paa";
			materialHL = "\A3\Characters_F_epb\Heads\Data\hl_white_20.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_white_hairy_2_co.paa";
			materialHL2 = "\A3\Characters_F_epb\Heads\Data\hl_white_20.rvmat";
		
		};
		class asczHead_slug_A3: WhiteHead_01
		{
			displayname = "Slug";
			texture = "ASCZ_Heads\data\Slug_co.paa";
			material = "ASCZ_Heads\data\Slug.rvmat";
			head = "GreekHead_A3";
			identityTypes[] = {"Head_Euro"};
			author = "Taurus";
			materialWounded1 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			materialWounded2 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			textureHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
			disabled = 0;
		};
		class asczHead_plumber_A3: WhiteHead_01
		{
			displayname = "Plumber";
			texture = "ASCZ_Heads\data\Plumber_co.paa";
			material = "ASCZ_Heads\data\Plumber.rvmat";
			head = "GreekHead_A3";
			identityTypes[] = {"Head_NATO","Head_Euro"};
			author = "Taurus";
			materialWounded1 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			materialWounded2 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			textureHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL = "\A3\Characters_F_epb\Heads\Data\hl_white_20.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL2 = "\A3\Characters_F_epb\Heads\Data\hl_white_20.rvmat";
			disabled = 0;
		};
		class asczHead_hammond_A3: WhiteHead_01
		{
			displayname = "Hammond";
			texture = "ASCZ_Heads\data\Hammond_co.paa";
			material = "ASCZ_Heads\data\Hammond.rvmat";
			head = "NATOHead_A3";
			identityTypes[] = {"Head_NATO","Head_Euro"};
			author = "Taurus";
			materialWounded1 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			materialWounded2 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			textureHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
			disabled = 0;
		};
		class asczHead_johnhoot_A3: WhiteHead_01
		{
			displayname = "Johnhoot";
			texture = "ASCZ_Heads\data\Johnhoot_co.paa";
			head = "NATOHead_A3";
			identityTypes[] = {"Head_Euro","Head_NATO"};
			author = "Taurus";
			material = "ASCZ_Heads\data\Logan.rvmat";
			disabled = 0;
			materialWounded1 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			materialWounded2 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			textureHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_1_co.paa";
			materialHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
		};
		class asczHead_grey_A3: AfricanHead_01
		{
			displayname = "Grey";
			texture = "ASCZ_Heads\data\Grey_co.paa";
			material = "ASCZ_Heads\data\Grey.rvmat";
			head = "BlackHead_A3";
			identityTypes[] = {"Head_NATO","Head_African","Head_Tanoan"};
			author = "Taurus";
			materialWounded1 = "A3\Characters_F_Exp\Heads\Data\m_tanoan_01_injury.rvmat";
			materialWounded2 = "A3\Characters_F_Exp\Heads\Data\m_tanoan_01_injury.rvmat";
			textureHL = "\A3\Characters_F_Exp\Heads\Data\hl_tanoan_bald_co.paa";
			materialHL = "\A3\Characters_F\Heads\Data\hl_black_bald_muscular.rvmat";
			textureHL2 = "\A3\Characters_F_Exp\Heads\Data\hl_tanoan_bald_co.paa";
			materialHL2 = "\A3\Characters_F\Heads\Data\hl_black_bald_muscular.rvmat";
            disabled = 0;
		};
		class asczHead_boredslav_A3: Kerry
		{
			displayname = "Boredslav";
			texture = "ASCZ_Heads\data\Boredslav_co.paa";
			head = "KerryHead_A3";
			identityTypes[] = {"Head_Euro"};
			author = "Taurus";
			material = "ASCZ_Heads\data\Boredslav.rvmat";
			disabled = 0;
			materialWounded1 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			materialWounded2 = "A3\Characters_F_epb\Heads\Data\m_White_20_injury.rvmat";
			textureHL = "\A3\Characters_F\Heads\Data\hl_white_hairy_2_co.paa";
			materialHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_white_hairy_2_co.paa";
			materialHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
		};
		class asczHead_boomlakhaoo_A3: AsianHead_A3_01
		{
			author = "Taurus";
			displayname = "Boomlakhaoo";
			identityTypes[] = {"Head_TK"};
			head = "GreekHead_A3";
			texture = "ASCZ_Heads\data\Boomlakhaoo_co.paa";
			material = "ASCZ_Heads\data\Boomlakhaoo.rvmat";
			textureHL = "\A3\Characters_F\Heads\Data\hl_black_bald_co.paa";
			materialHL = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_black_bald_co.paa";
			materialHL2 = "\A3\Characters_F\Heads\Data\hl_White_hairy_muscular.rvmat";
		};
		class asczHead_idoka_A3: AsianHead_A3_01
		{
			author = "Taurus";
			displayname = "Idoka";
			identityTypes[] = {"Head_Asian"};
			head = "AsianHead_A3";
			texture = "ASCZ_Heads\data\Idoka_co.paa";
			material = "ASCZ_Heads\data\Idoka.rvmat";
			materialWounded1 = "A3\Characters_F\Heads\Data\m_Asian_03_injury.rvmat";
			materialWounded2 = "A3\Characters_F\Heads\Data\m_Asian_03_injury.rvmat";
		};
	};
};