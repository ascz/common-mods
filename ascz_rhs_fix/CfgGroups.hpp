class CfgGroups
{
    class West
    {
        class rhs_faction_usarmy_d
        {
            name = "US Army";
            class rhs_group_nato_usarmy_d_infantry
            {
                name = "Infantry (UCP)";
            };
        };
        class rhs_faction_usarmy_wd
        {
            name = "US Army (Woodland)";
            class rhs_group_nato_usarmy_wd_infantry
            {
                name = "Infantry (OCP)";
            };
        };
        class rhs_faction_usmc_d
        {
            name = "USMC (Desert)";

        };
        class rhs_faction_usmc_wd
        {
            name = "USMC";

        };
        class rhs_faction_socom_marsoc
        {
            name = "SOCOM";
        };
    };
};