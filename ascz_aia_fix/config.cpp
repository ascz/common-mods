class CfgPatches
{
	class ascz_aia_fix
	{
	    units[] = {"Obstacle_saddle","RampConcrete","HumpsDirt","BuoySmall","BuoyBig","RoadBarrier_light","
	    ","Land_fort_bagfence_corner","Land_fort_bagfence_long","Land_fort_bagfence_round","Land_fort_artillery_nest","Land_fort_rampart","Fort_RazorWire","Fort_Crate_wood","Land_fortified_nest_big","Land_fortified_nest_small","Land_Fort_Watchtower","Fort_Barracks_USMC","Fort_Nest_M240","Hedgehog","Hhedgehog_concrete","Fort_EnvelopeSmall","Fort_EnvelopeBig","Fort_Barricade","FoldTable","FoldChair","Desk","SmallTable","Land_CncBlock","Land_CncBlock_Stripes","Land_CncBlock_D","Land_Toilet","Land_Fire","Misc_TyreHeap","Land_ladder","Land_ladder_half","Sr_border","Land_Barrel_empty","Land_Barrel_water","Land_Barrel_sand","Barrels","Barrel1","Barrel4","Barrel5","Paleta1","Paleta2","FlagCarrierUSA","HeliH","Land_Misc_deerstand","Land_Misc_GContainer_Big","Land_Misc_Scaffolding","Land_Ind_IlluminantTower","Land_Ind_Timbers","Land_Ind_BoardsPack1","Land_radar","Fence_Ind","Garbage_container","Garbage_can","Notice_board","Axe_woodblock","Haystack_small","Misc_concrete_High","ZavoraAnim","Fuel_can","PowerGenerator","Satelit","Land_psi_bouda","Land_Pneu","Misc_palletsfoiled_heap","Misc_palletsfoiled","Gunrack1","Sign_Danger","Sign_1L_Border","Land_arrows_desk_L","Land_arrows_desk_R","Land_arrows_yellow_R","Land_arrows_yellow_L","Land_coneLight","Land_RedWhiteBarrier","FlagCarrierChecked","FlagCarrierSmall","FlagCarrierCDF","FlagCarrierRU","FlagCarrierINS","FlagCarrierGUE","Sign_circle","SkeetMachine","SkeetDisk","MetalBucket","FloorMop","Baseball","Notebook","SmallTV","Suitcase","Can_small","Radio","SatPhone","EvPhoto","Explosive","Loudspeaker","Misc_Wall_lamp","Misc_Videoprojektor","Misc_Videoprojektor_platno","TargetPopUpTarget","TargetEpopup","TargetE","TargetGrenade","Land_A_tent","Land_tent_east","Camp","CampEast","ACamp","Land_GuardShed","Land_Antenna","Land_CamoNet_NATO","Land_CamoNetVar_NATO","Land_CamoNetB_NATO","Land_CamoNet_EAST","Land_CamoNetVar_EAST","Land_CamoNetB_EAST","76n6ClamShell","PowGen_Big","Barrack2","MASH","Misc_cargo_cont_small","Misc_cargo_cont_small2","Misc_cargo_cont_tiny","Misc_cargo_cont_net1","Misc_cargo_cont_net2","Misc_cargo_cont_net3","Misc_Backpackheap","Land_Climbing_Obstacle","Land_podlejzacka","Land_prolejzacka","Land_prebehlavka","Land_obihacka","Land_obstacle_get_over","Land_obstacle_prone","Land_obstacle_run_duck","Land_WoodenRamp","Land_ConcreteRamp","Land_ConcreteBlock","Land_Dirthump01","Land_Dirthump02","Land_Dirthump03","Land_Shooting_range","Land_BoatSmall_1","Land_BoatSmall_2a","Land_BoatSmall_2b","HMMWVWreck","BMP2Wreck","UralWreck","datsun01Wreck","datsun02Wreck","hiluxWreck","Mi8Wreck","T72Wreck","T72WreckTurret","BRDMWreck","UAZWreck","SKODAWreck","LADAWreck","Hanged","Hanged_MD","Grave","","Body","GraveCross1","GraveCross2","GraveCrossHelmet","Land_Church_tomb_1","Land_Church_tomb_2","Land_Church_tomb_3","Mass_grave","C130J_wreck_EP1", "UH60_wreck_EP1","GraveCross2_EP1","GraveCrossHelmet_EP1","Land_fort_artillery_nest_EP1","Land_fort_rampart_EP1","Land_fortified_nest_big_EP1","Land_fortified_nest_small_EP1","Land_Fort_Watchtower_EP1","Hedgehog_EP1","Fort_EnvelopeSmall_EP1","Fort_EnvelopeBig_EP1","Fort_Barricade_EP1","Land_CamoNet_NATO_EP1","Land_CamoNetVar_NATO_EP1","Land_CamoNetB_NATO_EP1","","Land_CamoNet_EAST_EP1","Land_CamoNetVar_EAST_EP1","Land_CamoNetB_EAST_EP1","76n6ClamShell_EP1","PowGen_Big_EP1","Land_Barrack2_EP1","MASH_EP1","Misc_cargo_cont_small_EP1","AmmoCrate_NoInteractive_","AmmoCrates_NoInteractive_Small","AmmoCrates_NoInteractive_Medium","AmmoCrates_NoInteractive_Large","Camp_EP1","CampEast_EP1","ACamp_EP1","C130J_static_EP1","Misc_TyreHeapEP1","Land_ladderEP1","Land_ladder_half_EP1","Land_radar_EP1","Notice_board_EP1","PowerGenerator_EP1","Sign_MP_blu_EP1","Sign_MP_op_EP1","Sign_MP_ind_EP1","Sign_1L_Firstaid_EP1","Sign_1L_Noentry_EP1","Sign_Checkpoint_US_EP1","Sign_Checkpoint_TK_EP1","Land_Dirthump01_EP1","Land_Dirthump02_EP1","Land_Dirthump03_EP1","TargetE_EP1","TargetFakeTank_Lockable_EP1","GunrackUS_EP1","Misc_Backpackheap_EP1","FlagPole_EP1","FlagCarrierUNO_EP1","FlagCarrierRedCrystal_EP1","FlagCarrierTFKnight_EP1","FlagCarrierCDFEnsign_EP1","FlagCarrierRedCross_EP1","FlagCarrierUSArmy_EP1","FlagCarrierTKMilitia_EP1","FlagCarrierRedCrescent_EP1","FlagCarrierGermany_EP1","FlagCarrierNATO_EP1","FlagCarrierBIS_EP1","FlagCarrierCzechRepublic_EP1","FlagCarrierPOWMIA_EP1","FlagCarrierBLUFOR_EP1","FlagCarrierOPFOR_EP1","FlagCarrierINDFOR_EP1","FlagCarrierTakistan_EP1","FlagCarrierTakistanKingdom_EP1","FlagCarrierUSA_EP1","FlagCarrierCDF_EP1","FlagCarrierWhite_EP1","Microphone1_ep1","ClutterCutter_EP1","","Land_Misc_Cargo1Eo_EP1","Land_Misc_Cargo1E_EP1","Sign_circle_EP1","Sign_sphere10cm_EP1","Sign_sphere25cm_EP1","Sign_sphere100cm_EP1","Sign_arrow_down_EP1","Sign_arrow_down_large_EP1","HMMWV_Ghost_EP1","Training_target_EP1","Land_Hlidac_Budka_EP1","Land_Ind_TankSmall2_EP1","Land_Misc_Cargo2E_EP1","Bleacher_EP1","WaterBasin_conc_EP1","Dirtmount_EP1","Infostand_1_EP1"};
        worlds[] = {};
        weapons[] = {};
		requiredVersion = 1.0;
		requiredAddons[] = {"AiA_Worlds","AiA_Core"};
		author[] = {"EvroMalarkey"};
		version = "1.0";
	};
};
class CfgWorlds
{
	class DefaultWorld
	{
		class Lighting;
		class DayLightingBrightAlmost;
		class DayLightingRainy;
		class Weather
		{
			class Overcast
			{
				class Weather1;
				class Weather2;
				class Weather3;
				class Weather4;
				class Weather5;
			};
		};
		class DefaultClutter;
	};
	class CAWorld: DefaultWorld
	{
        class Weather: Weather
        {
            class Overcast: Overcast
            {
                class Weather1: Weather1
                {
                    sky = "A3\Map_Stratis\Data\sky_veryclear_sky.paa";
                    skyR = "A3\Map_Stratis\Data\sky_veryclear_lco.paa";
                    horizon = "A3\Map_Stratis\Data\sky_veryclear_horizont_sky.paa";
                };
                class Weather7: Weather1
                {
                    sky = "A3\Map_Stratis\Data\sky_veryclear_sky.paa";
                    skyR = "A3\Map_Stratis\Data\sky_clear_lco.paa";
                    horizon = "A3\Map_Stratis\Data\sky_clear_horizont_sky.paa";
                };
                class Weather2: Weather2
                {
                    sky = "A3\Map_Stratis\Data\sky_veryclear_sky.paa";
                    skyR = "A3\Map_Stratis\Data\sky_almostclear_lco.paa";
                    horizon = "A3\Map_Stratis\Data\sky_almostclear_horizont_sky.paa";
                };
                class Weather3: Weather3
                {
                    sky = "A3\Map_Stratis\Data\sky_veryclear_sky.paa";
                    skyR = "A3\Map_Stratis\Data\sky_semicloudy_lco.paa";
                    horizon = "A3\Map_Stratis\Data\sky_semicloudy_horizont_sky.paa";
                };
                class Weather4: Weather4
                {
                    sky = "A3\Map_Stratis\Data\sky_veryclear_sky.paa";
                    skyR = "A3\Map_Stratis\Data\sky_cloudy_lco.paa";
                    horizon = "A3\Map_Stratis\Data\sky_cloudy_horizont_sky.paa";
                };
                class Weather5: Weather5
                {
                    sky = "A3\Map_Stratis\Data\sky_veryclear_sky.paa";
                    skyR = "A3\Map_Stratis\Data\sky_mostlycloudy_lco.paa";
                    horizon = "A3\Map_Stratis\Data\sky_mostlycloudy_horizont_sky.paa";
                };
            };
        };
        class Grid;
    };
    class Chernarus: CAWorld
    {
		cutscenes[] = {"ChernarusIntro"};
        class clutter
        {
            class GrassCrooked: DefaultClutter
            {
                scaleMin = 0.4;
                scaleMax = 0.7;
            };
            class GrassCrookedGreen: DefaultClutter
            {
                scaleMin = 0.4;
                scaleMax = 0.6;
            };
            class GrassCrookedForest: DefaultClutter
            {
                scaleMin = 0.4;
                scaleMax = 0.6;
            };
            class FernAutumn: DefaultClutter
            {
                scaleMax = 0.7;
            };
            class FernAutumnTall: DefaultClutter
            {
                scaleMin = 0.3;
                scaleMax = 0.5;
            };
        };
    };
    class Bootcamp_ACR: CAWorld
    {
		cutscenes[] = {"Bootcamp_ACRIntro"};
        class clutter
        {
            class GrassCrookedSmall: DefaultClutter
            {
                scaleMin = 0.4;
                scaleMax = 0.6;
            };
            class GrassCrookedGreen: DefaultClutter
            {
                scaleMin = 0.5;
                scaleMax = 0.7;
            };
            class GrassCrookedGreenSmall: DefaultClutter
            {
                scaleMin = 0.4;
                scaleMax = 0.6;
            };
            class GrassCrookedForest: DefaultClutter
            {
                scaleMin = 0.4;
                scaleMax = 0.6;
            };
            class FernAutumn: DefaultClutter
            {
                scaleMin = 0.4;
                scaleMax = 0.7;
            };
            class FernAutumnTall: DefaultClutter
            {
                scaleMin = 0.3;
                scaleMax = 0.6;
            };
        };
    };
    class Desert_E: CAWorld
    {
		cutscenes[] = {"Desert_EIntro"};
    };
    class Intro: CAWorld
    {
		cutscenes[] = {"IntroIntro"};
        class Grid: Grid
        {
            offsetX = 0;
            offsetY = 0;
            class Zoom1
            {
                zoomMax = 0.15;
                format = "XY";
                formatX = "000";
                formatY = "000";
                stepX = 100;
                stepY = 100;
            };
            class Zoom2
            {
                zoomMax = 0.85;
                format = "XY";
                formatX = "00";
                formatY = "00";
                stepX = 1000;
                stepY = 1000;
            };
            class Zoom3
            {
                zoomMax = 1e+030;
                format = "XY";
                formatX = "0";
                formatY = "0";
                stepX = 10000;
                stepY = 10000;
            };
        };
    };
    class Porto: CAWorld
    {
		cutscenes[] = {"PortoIntro"};
        class Grid: Grid
        {
            offsetX = 0;
            offsetY = 0;
            class Zoom1
            {
                zoomMax = 0.15;
                format = "XY";
                formatX = "000";
                formatY = "000";
                stepX = 100;
                stepY = 100;
            };
            class Zoom2
            {
                zoomMax = 0.85;
                format = "XY";
                formatX = "00";
                formatY = "00";
                stepX = 1000;
                stepY = 1000;
            };
            class Zoom3
            {
                zoomMax = 1e+030;
                format = "XY";
                formatX = "0";
                formatY = "0";
                stepX = 10000;
                stepY = 10000;
            };
        };
    };
    class ProvingGrounds_PMC: CAWorld
    {
		cutscenes[] = {"ProvingGrounds_PMCIntro"};
    };
    class Sara: CAWorld
    {
		cutscenes[] = {"SaraIntro"};
        class Grid: Grid
        {
            offsetX = 0;
            offsetY = 0;
            class Zoom1
            {
                zoomMax = 0.15;
                format = "XY";
                formatX = "000";
                formatY = "000";
                stepX = 100;
                stepY = 100;
            };
            class Zoom2
            {
                zoomMax = 0.85;
                format = "XY";
                formatX = "00";
                formatY = "00";
                stepX = 1000;
                stepY = 1000;
            };
            class Zoom3
            {
                zoomMax = 1e+030;
                format = "XY";
                formatX = "0";
                formatY = "0";
                stepX = 10000;
                stepY = 10000;
            };
        };
    };
    class Shapur_BAF: CAWorld
    {
		cutscenes[] = {"Shapur_BAFIntro"};
    };
    class Takistan: CAWorld
    {
		cutscenes[] = {"TakistanIntro"};
    };
    class Utes: CAWorld
    {
		cutscenes[] = {"UtesIntro"};
    };
    class Woodland_ACR: CAWorld
    {
		cutscenes[] = {"Woodland_ACRIntro"};
        class clutter
        {
            class GrassCrooked: DefaultClutter
            {
                scaleMin = 0.5;
                scaleMax = 0.7;
            };
            class GrassCrookedGreen: DefaultClutter
            {
                scaleMin = 0.4;
                scaleMax = 0.6;
            };
            class GrassCrookedForest: DefaultClutter
            {
                scaleMin = 0.4;
                scaleMax = 0.6;
            };
            class FernAutumn: DefaultClutter
            {
                scaleMin = 0.3;
                scaleMax = 0.5;
            };
            class FernAutumnTall: DefaultClutter
            {
                scaleMin = 0.4;
                scaleMax = 0.6;
            };
        };
    };
    class Zargabad: CAWorld
    {
		cutscenes[] = {"ZargabadIntro"};
    };
	class Chernarus_Summer: Chernarus
	{
		description = "$STR_SUMMER_CHERNARUS";
		cutscenes[] = {"Chernarus_SummerIntro"};
	};
	class Sara_dbe1: CAWorld
	{
		description = "$STR_UNITED_SAHRANI";
		cutscenes[] = {"Sara_dbe1Intro"};
        class Grid: Grid
        {
            offsetX = 0;
            offsetY = 0;
            class Zoom1
            {
                zoomMax = 0.15;
                format = "XY";
                formatX = "000";
                formatY = "000";
                stepX = 100;
                stepY = 100;
            };
            class Zoom2
            {
                zoomMax = 0.85;
                format = "XY";
                formatX = "00";
                formatY = "00";
                stepX = 1000;
                stepY = 1000;
            };
            class Zoom3
            {
                zoomMax = 1e+030;
                format = "XY";
                formatX = "0";
                formatY = "0";
                stepX = 10000;
                stepY = 10000;
            };
        };
	};
	class SaraLite: CAWorld
	{
		description = "$STR_SOUTHERN_SAHRANI";
		cutscenes[] = {"SaraLiteIntro"};
        class Grid: Grid
        {
            offsetX = 0;
            offsetY = 0;
            class Zoom1
            {
                zoomMax = 0.15;
                format = "XY";
                formatX = "000";
                formatY = "000";
                stepX = 100;
                stepY = 100;
            };
            class Zoom2
            {
                zoomMax = 0.85;
                format = "XY";
                formatX = "00";
                formatY = "00";
                stepX = 1000;
                stepY = 1000;
            };
            class Zoom3
            {
                zoomMax = 1e+030;
                format = "XY";
                formatX = "0";
                formatY = "0";
                stepX = 10000;
                stepY = 10000;
            };
        };
	};
	class Mountains_ACR: CAWorld
	{
		description = "$STR_TAKISTAN_MOUNTAINS";
		cutscenes[] = {"Mountains_ACRIntro"};
	};
};

class CfgMissions
{
	class Cutscenes
	{
		class ChernarusIntro
		{
			directory = "ascz_aia_fix\Scenes\intro.Chernarus";
		};
		class Bootcamp_ACRIntro
		{
			directory = "ascz_aia_fix\Scenes\intro.Bootcamp_ACR";
		};
		class Desert_EIntro
		{
			directory = "ascz_aia_fix\Scenes\intro.Desert_E";
		};
		class IntroIntro
		{
			directory = "ascz_aia_fix\Scenes\intro.Intro";
		};
		class PortoIntro
		{
			directory = "ascz_aia_fix\Scenes\intro.Porto";
		};
		class ProvingGrounds_PMCIntro
		{
			directory = "ascz_aia_fix\Scenes\intro.ProvingGrounds_PMC";
		};
		class SaraIntro
		{
			directory = "ascz_aia_fix\Scenes\intro.Sara";
		};
		class Shapur_BAFIntro
		{
			directory = "ascz_aia_fix\Scenes\intro.Shapur_BAF";
		};
		class TakistanIntro
		{
			directory = "ascz_aia_fix\Scenes\intro.Takistan";
		};
		class UtesIntro
		{
			directory = "ascz_aia_fix\Scenes\intro.utes";
		};
		class Woodland_ACRIntro
		{
			directory = "ascz_aia_fix\Scenes\intro.Woodland_ACR";
		};
		class ZargabadIntro
		{
			directory = "ascz_aia_fix\Scenes\intro.Zargabad";
		};
		class Chernarus_SummerIntro
		{
			directory = "ascz_aia_fix\Scenes\intro.Chernarus_Summer";
		};
		class Sara_dbe1Intro
		{
			directory = "ascz_aia_fix\Scenes\intro.Sara_dbe1";
		};
		class SaraLiteIntro
		{
			directory = "ascz_aia_fix\Scenes\intro.SaraLite";
		};
		class Mountains_ACRIntro
		{
			directory = "ascz_aia_fix\Scenes\intro.Mountains_ACR";
		};
	};
};

class CfgVehicles {
class Thing;
	class Strategic;
	class NonStrategic;
	class House;
	class Static;
    class Wall;
	class Wall1: Wall
	{
		scopeCurator = 2;
	};
	class Land_Zidka_branka: Wall
	{
		scopeCurator = 2;
	};
	class Wallend: Wall
	{
		scopeCurator = 2;
	};
	class Land_Zidka03: Wall
	{
		scopeCurator = 2;
	};
    class Land_Hlidac_budka: House
    {
        scopeCurator = 2;
	};
	class WireFence: NonStrategic
	{
		scopeCurator = 2;
	};
	class Fortress1: Strategic
	{
		scopeCurator = 2;
	};
	class Shed: Strategic
	{
		scopeCurator = 2;
	};
	class Obstacle_saddle: NonStrategic
	{
		scopeCurator = 2;
	};
	class RampConcrete: NonStrategic
	{
		scopeCurator = 2;
	};
	class HumpsDirt: NonStrategic
	{
		scopeCurator = 2;
	};
	class Ship;
	class BuoySmall: Ship
	{
		scopeCurator = 2;
	};
	class BuoyBig: Ship
	{
		scopeCurator = 2;
	};
	class RoadBarrier_light: NonStrategic
	{
		scopeCurator = 2;
	};
	class Land_Hangar_2: House
    {
        scopeCurator = 2;
    };
	class Land_podlejzacka: Strategic
    {
        scopeCurator = 2;
	};
	class BarrelBase;
	class Barrels: BarrelBase
	{
        scopeCurator = 2;
	};
	class Barrel1: BarrelBase
	{
        scopeCurator = 2;
	};
	class Barrel2: BarrelBase
	{
        scopeCurator = 2;
	};
	class Barrel3: BarrelBase
	{
        scopeCurator = 2;
	};
	class Barrel4: BarrelBase
	{
        scopeCurator = 2;
	};
	class Barrel5: BarrelBase
	{
        scopeCurator = 2;
	};
	class Barrel6: BarrelBase
	{
        scopeCurator = 2;
	};
	class Camera1: Thing
	{
        scopeCurator = 2;
	};
	class PaletaBase: NonStrategic{};
	class Paleta1: PaletaBase
	{
        scopeCurator = 2;
	};
	class Paleta2: PaletaBase
	{
        scopeCurator = 2;
	};
	class JeepWreck1: Strategic
	{
        scopeCurator = 2;
	};
	class JeepWreck3: JeepWreck1
	{
        scopeCurator = 2;
	};
	class misc01: NonStrategic
	{
        scopeCurator = 2;
	};
	class FlagCarrier;
	class FlagCarrierWest: FlagCarrier
	{
        scopeCurator = 2;
	};
	class Danger: FlagCarrier
	{
        scopeCurator = 2;
	};
	class DangerWest: FlagCarrier
	{
        scopeCurator = 2;
	};
	class DangerEAST: FlagCarrier
	{
        scopeCurator = 2;
	};
	class DangerGUE: FlagCarrier
	{
        scopeCurator = 2;
	};
	class Fence: NonStrategic
	{
        scopeCurator = 2;
	};
	class FenceWood: Fence
	{
        scopeCurator = 2;
	};
	class FenceWoodPalet: Fence
	{
        scopeCurator = 2;
	};
	class Wire: Fence
	{
        scopeCurator = 2;
	};
	class Fire: NonStrategic
	{
        scopeCurator = 2;
	};
	class HeliH: NonStrategic
	{
        scopeCurator = 2;
	};
	class TargetTraining: NonStrategic
	{
        scopeCurator = 2;
	};
	class Land_ladder: House
	{
        scopeCurator = 2;
	};
	class Land_ladder_half: House
	{
        scopeCurator = 2;
	};
	class ClutterCutter: Thing
	{
        scopeCurator = 2;
	};
	class snowman: Static
	{
        scopeCurator = 2; // Default: 1
	};
	class snow: Static
	{
        scopeCurator = 2; // Default: 1
	};
	
	class Land_HBarrier_large: NonStrategic
    {
        scopeCurator = 2;
    };
    class Land_fort_bagfence_corner: House
    {
        scopeCurator = 2;
    };
    class Land_fort_bagfence_long: House
    {
        scopeCurator = 2;
    };
    class Land_fort_bagfence_round: House
    {
        scopeCurator = 2;
    };
    class Land_fort_artillery_nest: House
    {
        scopeCurator = 2;
    };
    class Land_fort_rampart: House
    {
        scopeCurator = 2;
    };
    class Fort_RazorWire: NonStrategic
    {
        scopeCurator = 2;
    };
    class Fort_Crate_wood: NonStrategic
    {
        scopeCurator = 2;
    };
    class Land_fortified_nest_big: House
    {
        scopeCurator = 2;
    };
    class Land_fortified_nest_small: House
    {
        scopeCurator = 2;
    };
    class Land_Fort_Watchtower: House
    {
        scopeCurator = 2;
    };
    class WarfareBBaseStructure: House{};
    class Fort_Barracks_USMC: WarfareBBaseStructure
    {
        scopeCurator = 2;
    };
    class StaticMGWeapon;
    class WarfareBMGNest_M240_base: StaticMGWeapon{};
    class Fort_Nest_M240: WarfareBMGNest_M240_base
    {
        scopeCurator = 2;
    };
    class Hedgehog: Strategic
    {
        scopeCurator = 2;
    };
    class Hhedgehog_concrete: Strategic
    {
        scopeCurator = 2;
    };
    class Fort_EnvelopeSmall: House
    {
        scopeCurator = 2;
    };
    class Fort_EnvelopeBig: House
    {
        scopeCurator = 2;
    };
    class Fort_Barricade: Strategic
    {
        scopeCurator = 2;
    };
    class FoldTable: Thing
    {
        scopeCurator = 2;
    };
    class FoldChair: Thing
    {
        scopeCurator = 2;
    };
    class Desk: Thing
    {
        scopeCurator = 2;
    };
    class SmallTable: Thing
    {
        scopeCurator = 2;
    };
    class Land_CncBlock: NonStrategic
    {
        scopeCurator = 2;
    };
    class Land_CncBlock_Stripes: NonStrategic
    {
        scopeCurator = 2;
    };
    class Land_CncBlock_D: NonStrategic
    {
        scopeCurator = 2;
    };
    class Land_Toilet: NonStrategic
    {
        scopeCurator = 2;
    };
    class Land_Fire: House
    {
        scopeCurator = 2;
    };
    class Misc_TyreHeap: Land_Fire
    {
        scopeCurator = 2;
    };
    class Sr_border: NonStrategic
    {
        scopeCurator = 2;
    };
    class Land_Barrel_empty: Thing
    {
        scopeCurator = 2;
    };
    class Land_Barrel_water: Thing
    {
        scopeCurator = 2;
    };
    class Land_Barrel_sand: Thing
    {
        scopeCurator = 2;
    };
    class FlagCarrierUSA: FlagCarrier
    {
        scopeCurator = 2;
    };
	class FlagCarrierBAF: FlagCarrier
    {
        scopeCurator = 2;
    };
    class Land_Misc_deerstand: House
    {
        scopeCurator = 2;
    };
    class Land_Misc_GContainer_Big: House
    {
        scopeCurator = 2;
    };
    class Land_Misc_Scaffolding: House
    {
        scopeCurator = 2;
    };
    class Land_Ind_IlluminantTower: House
    {
        scopeCurator = 2;
    };
    class Land_Ind_Timbers: House
    {
        scopeCurator = 2;
    };
    class Land_Ind_BoardsPack1: House
    {
        scopeCurator = 2;
    };
    class Land_vysilac_FM2;
    class Land_radar: Land_vysilac_FM2
    {
        scopeCurator = 2;
    };
    class Fence_Ind: Fence
    {
        scopeCurator = 2;
    };
    class Garbage_container: Strategic
    {
        scopeCurator = 2;
    };
    class Garbage_can: Strategic
    {
        scopeCurator = 2;
    };
    class Misc_thing;
    class Notice_board: Misc_thing
    {
        scopeCurator = 2;
    };
    class Axe_woodblock: Misc_thing
    {
        scopeCurator = 2;
    };
    class Haystack_small: Misc_thing
    {
        scopeCurator = 2;
    };
    class Misc_concrete_High: Misc_thing
    {
        scopeCurator = 2;
    };
    class ZavoraAnim: NonStrategic
    {
        scopeCurator = 2;
    };
    class Fuel_can: Misc_thing
    {
        scopeCurator = 2;
    };
    class PowerGenerator: Misc_thing
    {
        scopeCurator = 2;
    };
    class Satelit: Misc_thing
    {
        scopeCurator = 2;
    };
    class Land_psi_bouda: House
    {
        scopeCurator = 2;
    };
    class Land_Pneu: Thing
    {
        scopeCurator = 2;
    };
    class Misc_thing_NoInteractive;
    class Misc_palletsfoiled_heap: Misc_thing_NoInteractive
    {
        scopeCurator = 2;
    };
    class Misc_palletsfoiled: Misc_thing_NoInteractive
    {
        scopeCurator = 2;
    };
    class Gunrack1: NonStrategic
    {
        scopeCurator = 2;
    };
    class Sign_Danger: Thing
    {
        scopeCurator = 2;
    };
    class Sign_1L_Border: Thing
    {
        scopeCurator = 2;
    };
    class Land_arrows_desk_L: Thing
    {
        scopeCurator = 2;
    };
    class Land_arrows_desk_R: Thing
    {
        scopeCurator = 2;
    };
    class Land_arrows_yellow_R: Thing
    {
        scopeCurator = 2;
    };
    class Land_arrows_yellow_L: Thing
    {
        scopeCurator = 2;
    };
    class Land_coneLight: Thing
    {
        scopeCurator = 2;
    };
    class Land_RedWhiteBarrier: House
    {
        scopeCurator = 2;
    };
    class FlagCarrierCore;
    class FlagCarrierChecked: FlagCarrierCore
    {
        scopeCurator = 2;
    };
    class FlagCarrierSmall: NonStrategic
    {
        scopeCurator = 2;
    };
    class Sign_circle: Thing
    {
        scopeCurator = 2;
    };
    class Small_items;
    class SkeetMachine: Small_items
    {
        scopeCurator = 2;
    };
    class SkeetDisk: Small_items
    {
        scopeCurator = 2;
    };
    class MetalBucket: Small_items
    {
        scopeCurator = 2;
    };
    class FloorMop: Small_items
    {
        scopeCurator = 2;
    };
    class Baseball: Small_items
    {
        scopeCurator = 2;
    };
    class Notebook: Small_items
    {
        scopeCurator = 2;
    };
    class SmallTV: Small_items
    {
        scopeCurator = 2;
    };
    class Suitcase: Small_items
    {
        scopeCurator = 2;
    };
    class Can_small: Small_items
    {
        scopeCurator = 2;
    };
    class Radio: Small_items
    {
        scopeCurator = 2;
    };
    class SatPhone: Small_items
    {
        scopeCurator = 2;
    };
    class EvPhoto: Small_items
    {
        scopeCurator = 2;
    };
    class Small_items_NoInteractive;
    class Explosive: Small_items_NoInteractive
    {
        scopeCurator = 2;
    };
    class Loudspeaker: Small_items_NoInteractive
    {
        scopeCurator = 2;
    };
    class Misc_Wall_lamp: Small_items_NoInteractive
    {
        scopeCurator = 2;
    };
    class Misc_Videoprojektor: Small_items_NoInteractive
    {
        scopeCurator = 2;
    };
    class Misc_Videoprojektor_platno: Small_items_NoInteractive
    {
        scopeCurator = 2;
    };
    class TargetBase;
    class TargetPopUpTarget: TargetBase
    {
        scopeCurator = 2;
    };
    class TargetEpopup: TargetBase
    {
        scopeCurator = 2;
    };
    class TargetE: TargetBase
    {
        scopeCurator = 2;
    };
    class TargetGrenadBase;
    class TargetGrenade: TargetGrenadBase
    {
        scopeCurator = 2;
    };
    class Land_A_tent: House
    {
        scopeCurator = 2;
    };
    class Land_tent_east: House
    {
        scopeCurator = 2;
    };
    class Camp_base;
    class Camp: Camp_base
    {
        scopeCurator = 2;
    };
    class CampEast: Camp_base
    {
        scopeCurator = 2;
    };
    class ACamp: Camp_base
    {
        scopeCurator = 2;
    };
    class Land_GuardShed: House
    {
        scopeCurator = 2;
    };
    class Land_Antenna: House
    {
        scopeCurator = 2;
    };
    class Land_CamoNet_NATO: House
    {
        scopeCurator = 2;
    };
    class Land_CamoNet_EAST: House
    {
        scopeCurator = 2;
    };
    class 76n6ClamShell: Strategic
    {
        scopeCurator = 2;
    };
    class PowGen_Big: House
    {
        scopeCurator = 2;
    };
    class Land_Barrack2;
    class Barrack2: Land_Barrack2
    {
        scopeCurator = 2;
    };
    class MASH: Camp_base
    {
        scopeCurator = 2;
    };
    class Military_Item_NoInteractive;
    class Misc_cargo_cont_small: Military_Item_NoInteractive
    {
        scopeCurator = 2;
    };
    class Misc_cargo_cont_small2: Military_Item_NoInteractive
    {
        scopeCurator = 2;
    };
    class Misc_cargo_cont_tiny: Military_Item_NoInteractive
    {
        scopeCurator = 2;
    };
    class Misc_cargo_cont_net1: Military_Item_NoInteractive
    {
        scopeCurator = 2;
    };
    class Misc_cargo_cont_net2: Military_Item_NoInteractive
    {
        scopeCurator = 2;
    };
    class Misc_cargo_cont_net3: Military_Item_NoInteractive
    {
        scopeCurator = 2;
    };
    class Misc_Backpackheap: Military_Item_NoInteractive
    {
        scopeCurator = 2;
    };
    class Land_Climbing_Obstacle: House
    {
        scopeCurator = 2;
    };
    class Land_WoodenRamp: House
    {
        scopeCurator = 2;
    };
    class Land_ConcreteRamp: House
    {
        scopeCurator = 2;
    };
    class Land_ConcreteBlock: House
    {
        scopeCurator = 2;
    };
    class Land_Dirthump01: House
    {
        scopeCurator = 2;
    };
    class Land_Dirthump02: House
    {
        scopeCurator = 2;
    };
    class Land_Dirthump03: House
    {
        scopeCurator = 2;
    };
    class Land_Shooting_range: House
    {
        scopeCurator = 2;
    };
    class Land_BoatSmall_1: House
    {
        scopeCurator = 2;
    };
    class Wreck_Base;
    class HMMWVWreck: Wreck_Base
    {
        scopeCurator = 2;
    };
    class BMP2Wreck: Wreck_Base
    {
        scopeCurator = 2;
    };
    class UralWreck: Wreck_Base
    {
        scopeCurator = 2;
    };
    class datsun01Wreck: Wreck_Base
    {
        scopeCurator = 2;
    };
    class datsun02Wreck: Wreck_Base
    {
        scopeCurator = 2;
    };
    class hiluxWreck: Wreck_Base
    {
        scopeCurator = 2;
    };
    class Mi8Wreck: Wreck_Base
    {
        scopeCurator = 2;
    };
    class T72Wreck: Wreck_Base
    {
        scopeCurator = 2;
    };
    class T72WreckTurret: Wreck_Base
    {
        scopeCurator = 2;
    };
    class BRDMWreck: Wreck_Base
    {
        scopeCurator = 2;
    };
    class UAZWreck: Wreck_Base
    {
        scopeCurator = 2;
    };
    class SKODAWreck: Wreck_Base
    {
        scopeCurator = 2;
    };
    class LADAWreck: Wreck_Base
    {
        scopeCurator = 2;
    };
    class Hanged: NonStrategic
    {
        scopeCurator = 2;
    };
    class Grave: NonStrategic
    {
        scopeCurator = 2;
    };
    class Body: Grave
    {
        scopeCurator = 2;
    };
    class GraveCross1: Grave
    {
        scopeCurator = 2;
    };
    class GraveCross2: Grave
    {
        scopeCurator = 2;
    };
    class GraveCrossHelmet: Grave
    {
        scopeCurator = 2;
    };
    class Land_Church_tomb_1: Grave
    {
        scopeCurator = 2;
    };
    class Land_Church_tomb_2: Grave
    {
        scopeCurator = 2;
    };
    class Land_Church_tomb_3: Grave
    {
        scopeCurator = 2;
    };
    class Mass_grave: Grave
    {
        scopeCurator = 2;
    };


	class C130J_wreck_EP1: House
	{
		scopeCurator = 2;
	};
	class All;
	class AllVehicles: All
	{
		class NewTurret;
	};
	class Land: AllVehicles{};
	class LandVehicle: Land{};
	class StaticWeapon: LandVehicle
	{
		class Turrets
		{
			class MainTurret: NewTurret{};
		};
		class AnimationSources;
	};
	class UH60_wreck_EP1: StaticWeapon
	{
		scopeCurator = 2;
	};
	class GraveCross2_EP1: GraveCross2
	{
		scopeCurator = 2;
	};
	class GraveCrossHelmet_EP1: GraveCrossHelmet
	{
		scopeCurator = 2;
	};
	class Land_fort_artillery_nest_EP1: Land_fort_artillery_nest
	{
		scopeCurator = 2;
	};
	class Land_fort_rampart_EP1: Land_fort_rampart
	{
		scopeCurator = 2;
	};
	class Land_fortified_nest_big_EP1: Land_fortified_nest_big
	{
		scopeCurator = 2;
	};
	class Land_fortified_nest_small_EP1: Land_fortified_nest_small
	{
		scopeCurator = 2;
	};
	class Land_Fort_Watchtower_EP1: Land_Fort_Watchtower
	{
		scopeCurator = 2;
	};
	class Hedgehog_EP1: Hedgehog
	{
		scopeCurator = 2;
	};
	class Fort_EnvelopeSmall_EP1: Fort_EnvelopeSmall
	{
		scopeCurator = 2;
	};
	class Fort_EnvelopeBig_EP1: Fort_EnvelopeBig
	{
		scopeCurator = 2;
	};
	class Fort_Barricade_EP1: Fort_Barricade
	{
		scopeCurator = 2;
	};
	class Land_CamoNet_NATO_EP1: Land_CamoNet_NATO
	{
		scopeCurator = 2;
	};
    class Land_CamoNetVar_NATO;
	class Land_CamoNetVar_NATO_EP1: Land_CamoNetVar_NATO
	{
		scopeCurator = 2;
	};
    class Land_CamoNetB_NATO;
	class Land_CamoNetB_NATO_EP1: Land_CamoNetB_NATO
	{
		scopeCurator = 2;
	};
	class Land_CamoNet_EAST_EP1: Land_CamoNet_EAST
	{
		scopeCurator = 2;
	};
    class Land_CamoNetVar_EAST;
	class Land_CamoNetVar_EAST_EP1: Land_CamoNetVar_EAST
	{
		scopeCurator = 2;
	};
    class Land_CamoNetB_EAST;
	class Land_CamoNetB_EAST_EP1: Land_CamoNetB_EAST
	{
		scopeCurator = 2;
	};
	class 76n6ClamShell_EP1: 76n6ClamShell
	{
		scopeCurator = 2;
	};
	class PowGen_Big_EP1: PowGen_Big
	{
		scopeCurator = 2;
	};
	class Land_Barrack2_EP1: Barrack2
	{
		scopeCurator = 2;
	};
	class MASH_EP1: MASH
	{
		scopeCurator = 2;
	};
	class Misc_cargo_cont_small_EP1: Misc_cargo_cont_small
	{
		scopeCurator = 2;
	};
    class AmmoCrate_NoInteractive_Base_EP1;
	class AmmoCrate_NoInteractive_: AmmoCrate_NoInteractive_Base_EP1
	{
		scopeCurator = 2;
	};
	class AmmoCrates_NoInteractive_Small: AmmoCrate_NoInteractive_Base_EP1
	{
		scopeCurator = 2;
	};
	class AmmoCrates_NoInteractive_Medium: AmmoCrate_NoInteractive_Base_EP1
	{
		scopeCurator = 2;
	};
	class AmmoCrates_NoInteractive_Large: AmmoCrate_NoInteractive_Base_EP1
	{
		scopeCurator = 2;
	};
	class Camp_EP1: Camp
	{
		scopeCurator = 2;
	};
	class CampEast_EP1: CampEast
	{
		scopeCurator = 2;
	};
	class ACamp_EP1: ACamp
	{
		scopeCurator = 2;
	};
	class C130J_static_EP1: House
	{
		scopeCurator = 2;
	};
	class Misc_TyreHeapEP1: Misc_TyreHeap
	{
		scopeCurator = 2;
	};
	class Land_ladderEP1: Land_ladder
	{
		scopeCurator = 2;
	};
	class Land_ladder_half_EP1: Land_ladder_half
	{
		scopeCurator = 2;
	};
	class Land_radar_EP1: Land_radar
	{
		scopeCurator = 2;
	};
	class Notice_board_EP1: Notice_board
	{
		scopeCurator = 2;
	};
	class PowerGenerator_EP1: PowerGenerator
	{
		scopeCurator = 2;
	};
    class Sign_MP_blu;
	class Sign_MP_blu_EP1: Sign_MP_blu
	{
		scopeCurator = 2;
	};
    class Sign_MP_op;
	class Sign_MP_op_EP1: Sign_MP_op
	{
		scopeCurator = 2;
	};
    class Sign_MP_ind;
	class Sign_MP_ind_EP1: Sign_MP_ind
	{
		scopeCurator = 2;
	};
    class Sign_1L_Firstaid;
	class Sign_1L_Firstaid_EP1: Sign_1L_Firstaid
	{
		scopeCurator = 2;
	};
    class Sign_1L_Noentry;
	class Sign_1L_Noentry_EP1: Sign_1L_Noentry
	{
		scopeCurator = 2;
	};
    class Sign_Checkpoint;
	class Sign_Checkpoint_US_EP1: Sign_Checkpoint
	{
		scopeCurator = 2;
	};
	class Sign_Checkpoint_TK_EP1: Sign_Checkpoint
	{
		scopeCurator = 2;
	};
	class Land_Dirthump01_EP1: Land_Dirthump01
	{
		scopeCurator = 2;
	};
	class Land_Dirthump02_EP1: Land_Dirthump02
	{
		scopeCurator = 2;
	};
	class Land_Dirthump03_EP1: Land_Dirthump03
	{
		scopeCurator = 2;
	};
	class TargetE_EP1: TargetE
	{
		scopeCurator = 2;
	};
    class TargetFakeTank;
	class TargetFakeTank_Lockable_EP1: TargetFakeTank
	{
		scopeCurator = 2;
	};
	class GunrackUS_EP1: Gunrack1
	{
		scopeCurator = 2;
	};
	class Misc_Backpackheap_EP1: Misc_Backpackheap
	{
		scopeCurator = 2;
	};
	class FlagPole_EP1: FlagCarrier
	{
		scopeCurator = 2;
	};
	class FlagCarrierUNO_EP1: FlagCarrier
	{
		scopeCurator = 2;
	};
	class Microphone1_ep1: Small_items_NoInteractive
	{
		scopeCurator = 2;
	};
	class ClutterCutter_EP1: Thing
	{
		scopeCurator = 2;
	};
    class Land_Misc_Cargo1Bo_EP1;
	class Land_Misc_Cargo1Eo_EP1: Land_Misc_Cargo1Bo_EP1
	{
		scopeCurator = 2;
	};
	class Land_Misc_Cargo1E_EP1: House
	{
		scopeCurator = 2;
	};
    class Helper_Base_EP1;
	class Sign_circle_EP1: Helper_Base_EP1
	{
		scopeCurator = 2;
	};
	class Sign_sphere10cm_EP1: Helper_Base_EP1
	{
		scopeCurator = 2;
	};
	class Sign_sphere25cm_EP1: Helper_Base_EP1
	{
		scopeCurator = 2;
	};
	class Sign_sphere100cm_EP1: Helper_Base_EP1
	{
		scopeCurator = 2;
	};
	class Sign_arrow_down_EP1: Helper_Base_EP1
	{
		scopeCurator = 2;
	};
	class Sign_arrow_down_large_EP1: Helper_Base_EP1
	{
		scopeCurator = 2;
	};
	class HMMWV_Ghost_EP1: Helper_Base_EP1
	{
		scopeCurator = 2;
	};
	class Training_target_EP1: House
	{
		scopeCurator = 2;
	};
	class Land_Hlidac_Budka_EP1: Land_Hlidac_Budka
	{
		scopeCurator = 2;
	};
    class Land_Ind_TankSmall2;
	class Land_Ind_TankSmall2_EP1: Land_Ind_TankSmall2
	{
		scopeCurator = 2;
	};
	class Land_Misc_Cargo2E_EP1: House
	{
		scopeCurator = 2;
	};
	class Bleacher_EP1: House
	{
		scopeCurator = 2;
	};
	class WaterBasin_conc_EP1: House
	{
		scopeCurator = 2;
	};
	class Dirtmount_EP1: House
	{
		scopeCurator = 2;
	};
	class Infostand_1_EP1: Thing
	{
		scopeCurator = 2;
	};
	class ShootingRange_ACR: House
	{
		scopeCurator = 2;
	};
	class Marker_Basic_ACR: Helper_Base_EP1
	{
		scopeCurator = 2;
	};
	class Cube5cm_ACR: Helper_Base_EP1
	{
		scopeCurator = 2;
	};
	class Sign_DangerMines_ACR: Sign_1L_Border
	{
		scopeCurator = 2;
	};
	class TargetStatic_ACR: TargetE_EP1
	{
		scopeCurator = 2;
	};
	class TargetPopup_ACR: TargetEpopup
	{
		scopeCurator = 2;
	};
};