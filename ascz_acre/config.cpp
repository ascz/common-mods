class CfgPatches
{
	class ascz_acre
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {};
	};
};
class CfgFunctions
{
	class ascz
	{
		class ascz_acre
		{
            file = "\ascz_acre\functions";
			class check_acre
			{
				preInit = 1;
			};
		};
	};
};